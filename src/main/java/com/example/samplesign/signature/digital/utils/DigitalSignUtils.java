package com.example.samplesign.signature.digital.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DigitalSignUtils {

    public static List<String> getSignedName(AcroFields acroFields) {
        List<String> names = acroFields.getSignatureNames();
        int numberOfName = acroFields.getTotalRevisions();
        List<String> results = new ArrayList();

        for(int idx1 = numberOfName; idx1 >= 1; --idx1) {
            for (String name : names) {
                if (acroFields.getRevision((String) name) == idx1) {
                    results.add(name);
                }
            }
        }
        return results;
    }

    public static X509Certificate getCertificateFromCerFile(String filePath) throws CertificateException, FileNotFoundException {
        X509Certificate result = null;
        System.out.println("===filePath:" + filePath);
        if (filePath != null && filePath.trim().length() > 0) {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
            FileInputStream fileInputStream = new FileInputStream(filePath);
            result = (X509Certificate)certificateFactory.generateCertificate(fileInputStream);
        }
        return result;
    }

    public static PrivateKey getPrivateKey(String keyFile, String passphrase)
            throws OperatorCreationException, PKCSException, IOException {
        PrivateKey privateKey = null;
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        try (FileReader fileReader = new FileReader(keyFile)) {
            try (PEMParser pemParser = new PEMParser(fileReader)) {
                Object object = null;
                while ((object = pemParser.readObject()) != null) {
                    if (object instanceof PEMEncryptedKeyPair) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        PEMDecryptorProvider decProv =
                                new JcePEMDecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        var pair = converter.getKeyPair(((PEMEncryptedKeyPair) object)
                                .decryptKeyPair(decProv));
                        privateKey = pair.getPrivate();
                        break;
                    } else if (object instanceof PKCS8EncryptedPrivateKeyInfo) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        InputDecryptorProvider decryptionProv =
                                new JceOpenSSLPKCS8DecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        privateKey = converter.getPrivateKey(((PKCS8EncryptedPrivateKeyInfo) object)
                                .decryptPrivateKeyInfo(decryptionProv));
                        break;
                    } else if (object instanceof PEMKeyPair) {
                        var pair = converter.getKeyPair((PEMKeyPair) object);
                        privateKey = pair.getPrivate();
                        break;
                    }
                }
            }
        }
        return privateKey;
    }
    //add long term validation
    public static void addLtv(String src, String dest, OcspClient ocsp, CrlClient crl, TSAClient tsa) throws IOException,
            DocumentException, GeneralSecurityException {
        PdfReader r = new PdfReader(src);
        FileOutputStream fos = new FileOutputStream(dest);
        PdfStamper stp = PdfStamper.createSignature(r, fos, '\0', null, true);
        LtvVerification v = stp.getLtvVerification();
        AcroFields fields = stp.getAcroFields();
        List<String> names = fields.getSignatureNames();
        String sigName = names.get(names.size() - 1);
        PdfPKCS7 pkcs7 = fields.verifySignature(sigName);
        if (pkcs7.isTsp()) {
            System.out.println("TIMESTAMP!");
            v.addVerification(sigName, ocsp, crl,
                    LtvVerification.CertificateOption.SIGNING_CERTIFICATE,
                    LtvVerification.Level.CRL,
                    LtvVerification.CertificateInclusion.NO);
        } else {
            for (String name : names) {
                v.addVerification(name, ocsp, crl, LtvVerification.CertificateOption.WHOLE_CHAIN,
                        LtvVerification.Level.OCSP_CRL,
                        LtvVerification.CertificateInclusion.NO);
            }
        }
        PdfSignatureAppearance sap = stp.getSignatureAppearance();
        LtvTimestamp.timestamp(sap, tsa, null);
    }

    public static boolean verifyIntegrity(String path) throws IOException, GeneralSecurityException {
        PdfReader reader = new PdfReader(path);
        AcroFields fields = reader.getAcroFields();
        ArrayList<String> names = fields.getSignatureNames();
        for (String name : names) {
            System.out.println("===== " + name + " =====");
            PdfPKCS7 pkcs7 = fields.verifySignature(name);
            if (!pkcs7.verify()) return false;
        }
        return true;
    }

    public static PdfPKCS7 verifySignature(AcroFields fields, String name, KeyStore ks)
            throws GeneralSecurityException, IOException {
        PdfPKCS7 pkcs7 = fields.verifySignature(name);
        Certificate[] certs = pkcs7.getSignCertificateChain();
        Calendar cal = pkcs7.getSignDate();
        List<VerificationException> errors = CertificateVerification.verifyCertificates(certs, ks, cal);
        if (errors.size() == 0)
            System.out.println("Certificates verified against the KeyStore");
        else
            System.out.println(errors);
        X509Certificate signCert = (X509Certificate)certs[0];
        X509Certificate issuerCert = (certs.length > 1 ? (X509Certificate)certs[1] : null);
        System.out.println("=== Checking validity of the document at the time of signing ===");
        checkRevocation(pkcs7, signCert, issuerCert, cal.getTime());
        System.out.println("=== Checking validity of the document today ===");
        checkRevocation(pkcs7, signCert, issuerCert, new Date());
        return pkcs7;
    }

    // check time
    public void checkTimeValidInCertificate(X509Certificate cert, Date signDate) {
        try {
            cert.checkValidity(signDate);
            System.out
                    .println("The certificate was valid at the time of signing.");
        } catch (CertificateExpiredException e) {
            System.out
                    .println("The certificate was expired at the time of signing.");
        } catch (CertificateNotYetValidException e) {
            System.out
                    .println("The certificate wasn't valid yet at the time of signing.");
        }
        try {
            cert.checkValidity();
            System.out.println("The certificate is still valid.");
        } catch (CertificateExpiredException e) {
            System.out.println("The certificate has expired.");
        } catch (CertificateNotYetValidException e) {
            System.out.println("The certificate isn't valid yet.");
        }
    }

    public static void checkRevocation(PdfPKCS7 pkcs7, X509Certificate signCert, X509Certificate issuerCert, Date date) throws GeneralSecurityException, IOException {
        List<BasicOCSPResp> ocsps = new ArrayList<BasicOCSPResp>();
        if (pkcs7.getOcsp() != null)
            ocsps.add(pkcs7.getOcsp());
        OCSPVerifier ocspVerifier = new OCSPVerifier(null, ocsps);
        List<VerificationOK> verification =
                ocspVerifier.verify(signCert, issuerCert, date);
        if (verification.size() == 0) {
            List<X509CRL> crls = new ArrayList<X509CRL>();
            if (pkcs7.getCRLs() != null) {
                for (CRL crl : pkcs7.getCRLs())
                    crls.add((X509CRL)crl);
            }
            CRLVerifier crlVerifier = new CRLVerifier(null, crls);
            verification.addAll(crlVerifier.verify(signCert, issuerCert, date));
        }
        if (verification.size() == 0) {
            System.out.println("The signing certificate couldn't be verified");
        }
        else {
            for (VerificationOK v : verification)
                System.out.println(v);
        }
    }

    public static void main(String[] args) {
        String outputFileName = "results/sample_signed.pdf";
        Security.addProvider(new BouncyCastleProvider());
        //verify
        try {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            ks.setCertificateEntry("cacert",
                    cf.generateCertificate(new FileInputStream("cert/rootCA/root_X0F.crt")));
            ks.setCertificateEntry("test",
                    cf.generateCertificate(new FileInputStream("cert/test.crt")));
            PdfReader reader = new PdfReader(outputFileName);
            AcroFields fields = reader.getAcroFields();
            ArrayList<String> names = fields.getSignatureNames();
            for (String name : names) {
                System.out.println("===== " + name + " =====");
                PdfPKCS7 pkcs7 = DigitalSignUtils.verifySignature(fields,name,ks);
            }
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }
}
