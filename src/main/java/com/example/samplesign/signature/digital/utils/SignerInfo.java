package com.example.samplesign.signature.digital.utils;

import java.io.Serializable;

public class SignerInfo implements Serializable {
    private String userName;
    private Long userId;
    private Long userRoleId;

    public SignerInfo() {
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserRoleId() {
        return this.userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

}
