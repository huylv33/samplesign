package com.example.samplesign.signature.digital.utils;

import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

public class X509ExtensionUtil {
    private X509ExtensionUtil() {
    }
    public static String getCNInSubject(X509Certificate certificate) {
        try {
            X500Name x500Name = new JcaX509CertificateHolder(certificate).getSubject();
            RDN cn = x500Name.getRDNs(BCStyle.CN)[0];
            return IETFUtils.valueToString(cn.getFirst().getValue());
        } catch (CertificateEncodingException ex){
            return "";
        }
    }
}
