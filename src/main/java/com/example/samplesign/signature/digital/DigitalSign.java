package com.example.samplesign.signature.digital;

import com.example.samplesign.signature.digital.utils.DigitalSignUtils;
import com.example.samplesign.signature.digital.utils.SignerInfo;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.security.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.*;

public class DigitalSign implements Serializable {
    private static final Logger LOGGER = LogManager.getLogger(DigitalSign.class.getName());
    protected SignerInfo signer;
    protected Long errorCode;
    protected File originFile;
    protected String outputFileName;
    protected int estimatedSize = 0;
    protected Calendar calendar = null;
    protected byte[] hash = null;
    protected MakeSignature.CryptoStandard sigtype = null;
    protected ExternalDigest externalDigest = null;
    protected Collection<byte[]> crlBytes = null;
    protected PdfSignatureAppearance appearance = null;
    protected Collection<CrlClient> crlList = new ArrayList();
    protected byte[] ocsp = null;
    protected OcspClient ocspClient = null;
    protected TSAClient tsaClient = null;
    protected PdfPKCS7 pdfPKCS7 = null;
    protected Date hashingStartTime;
    protected Date hashingEndTime;
    protected Date writingStartTime;
    protected Date writingEndTime;

    public Date getWritingStartTime() {
        return this.writingStartTime;
    }

    public void setWritingStartTime(Date writingStartTime) {
        this.writingStartTime = writingStartTime;
    }

    public Date getWritingEndTime() {
        return this.writingEndTime;
    }

    public void setWritingEndTime(Date writingEndTime) {
        this.writingEndTime = writingEndTime;
    }

    public Date getHashingStartTime() {
        return this.hashingStartTime;
    }

    public void setHashingStartTime(Date hashingStartTime) {
        this.hashingStartTime = hashingStartTime;
    }

    public Date getHashingEndTime() {
        return this.hashingEndTime;
    }

    public void setHashingEndTime(Date hashingEndTime) {
        this.hashingEndTime = hashingEndTime;
    }

    public DigitalSign(DigitalSign digital) {
        this.signer = digital.signer;
        this.errorCode = digital.errorCode;
        this.originFile = digital.originFile;
        this.estimatedSize = digital.estimatedSize;
        this.calendar = digital.calendar;
        this.hash = digital.hash;
        this.sigtype = digital.sigtype;
        this.externalDigest = digital.externalDigest;
        this.crlBytes = digital.crlBytes;
        this.appearance = digital.appearance;
        this.crlList = digital.crlList;
        this.ocsp = digital.ocsp;
        this.ocspClient = digital.ocspClient;
        this.tsaClient = digital.tsaClient;
        this.pdfPKCS7 = digital.pdfPKCS7;
    }

    public Calendar getCalendarSign() {
        return this.appearance.getSignDate();
    }

    public DigitalSign(SignerInfo signer, String fullFileName, String outputFileName, boolean encrypted) {
        this.signer = signer;
        try {
            this.originFile = new File(fullFileName);
            if (this.originFile.exists()) {
                this.outputFileName = outputFileName;
                this.externalDigest = new BouncyCastleDigest();
            }
        } catch (Exception ex) {
            LOGGER.error("Lỗi constructor: " + ex.getMessage());
        }

    }

    public void setCrlBytes(Collection<byte[]> crlBytes) {
        this.crlBytes = crlBytes;
    }

    public void setCrlList(Collection<CrlClient> crlList) {
        this.crlList = crlList;
    }

    public void setOcsp(byte[] ocsp) {
        this.ocsp = ocsp;
    }

    public void setOcspClient(OcspClient ocspClient) {
        this.ocspClient = ocspClient;
    }

    public void setTsaClient(TSAClient tsaClient) {
        this.tsaClient = tsaClient;
    }

    private static Collection<byte[]> processCrl(Certificate cert, Collection<CrlClient> crlList) {
        if (crlList == null) {
            return null;
        } else {
            ArrayList<byte[]> crlBytes = new ArrayList();
            Iterator it = crlList.iterator();

            while(it.hasNext()) {
                CrlClient crlClient = (CrlClient)it.next();
                if (crlClient != null) {
                    Collection<byte[]> collection = crlClient.getEncoded((X509Certificate)cert, (String)null);
                    if (collection != null) {
                        crlBytes.addAll(collection);
                    }
                }
            }

            if (crlBytes.isEmpty()) {
                return null;
            } else {
                return crlBytes;
            }
        }
    }

    public byte[] getDigest(Certificate[] chain, String reason, String location, MakeSignature.CryptoStandard sigtype) {
        this.hashingStartTime = new Date();
        LOGGER.info("getDigest: begin");
        byte[] result = null;

        try {
            this.sigtype = sigtype;
            PdfReader reader = new PdfReader(new FileInputStream(this.originFile));
            PdfReader.unethicalreading = true;
            // add provider
            BouncyCastleProvider providerBC = new BouncyCastleProvider();
            Security.addProvider(providerBC);
            AcroFields acroFields = reader.getAcroFields();
            // lay danh sach chu ky
            List<String> signedNames = DigitalSignUtils.getSignedName(acroFields);
            this.hashingStartTime = new Date();
            // output file ký
            OutputStream dataOutputStream = new FileOutputStream(this.outputFileName);
            PdfStamper stamper;
            // kiem tra da co chu ky hay chua -> append hay khong
            if (signedNames.size() > 0) {
                stamper = PdfStamper.createSignature(reader, dataOutputStream, '\u0000', (File)null, true);
                LOGGER.info("getDigest: more than one sign");
            } else {
                stamper = PdfStamper.createSignature(reader, dataOutputStream, '\u0000');
                LOGGER.info("getDigest: first sign");
            }
            this.appearance = stamper.getSignatureAppearance();
            this.appearance.setReason(reason);
            this.appearance.setLocation(location);
            Certificate[] arr = chain;
            int length = chain.length;

            for(int i = 0; i < length; ++i) {
                Certificate certificate = arr[i];
                this.crlBytes = processCrl(certificate, this.crlList);
                if (this.crlBytes != null) {
                    break;
                }
            }

            if (this.estimatedSize == 0) {
                this.estimatedSize = 8192;
                byte[] element;
                if (this.crlBytes != null) {
                    for(Iterator it = this.crlBytes.iterator(); it.hasNext(); this.estimatedSize += element.length + 10) {
                        element = (byte[])it.next();
                    }
                }

                if (this.ocspClient != null) {
                    this.estimatedSize += 4192;
                }

                if (this.tsaClient != null) {
                    this.estimatedSize += 4192;
                }
            }

            this.appearance.setCertificate(chain[0]);
            PdfSignature cryptoDictionary = new PdfSignature(PdfName.ADOBE_PPKLITE, sigtype == MakeSignature.CryptoStandard.CADES ? PdfName.ETSI_CADES_DETACHED : PdfName.ADBE_PKCS7_DETACHED);
            // set reason, location, contact, sign date
            cryptoDictionary.setReason(this.appearance.getReason());
            cryptoDictionary.setLocation(this.appearance.getLocation());
            cryptoDictionary.setContact(this.appearance.getContact());
            cryptoDictionary.setDate(new PdfDate(this.appearance.getSignDate()));
            this.appearance.setCryptoDictionary(cryptoDictionary);
            HashMap<PdfName, Integer> exc = new HashMap();
            exc.put(PdfName.CONTENTS, this.estimatedSize * 2 + 2);
            this.appearance.preClose(exc);
            // chon thuat toan bam SHA256
            String hashAlgorithm = DigestAlgorithms.SHA256;
            this.pdfPKCS7 = new PdfPKCS7((PrivateKey)null, chain, hashAlgorithm, (String)null, this.externalDigest, false);
            InputStream data = this.appearance.getRangeStream();
            this.hash = DigestAlgorithms.digest(data, this.externalDigest.getMessageDigest(hashAlgorithm));
            this.calendar = Calendar.getInstance();
            if (chain.length >= 2 && this.ocspClient != null) {
                // ocsp response
                this.ocsp = this.ocspClient.getEncoded((X509Certificate)chain[0], (X509Certificate)chain[1], (String)null);
            }
            result = this.pdfPKCS7.getAuthenticatedAttributeBytes(this.hash, this.ocsp, this.crlBytes, sigtype);;
            this.hashingEndTime = new Date();
        } catch (DocumentException | GeneralSecurityException | IOException ex) {
            this.hashingEndTime = new Date();
            LOGGER.error("getDigest: " + ex.getMessage());
        }

        LOGGER.info("getDigest: end");
        return result;
    }

    public boolean appendSignature(byte[] extSignature) throws DocumentException, IOException {
        boolean result = false;
        LOGGER.info("appendSignature: begin---------------------------------");

        try {
            if (this.appearance != null && this.pdfPKCS7 != null && this.hash != null && extSignature != null && this.estimatedSize > 0) {
                this.pdfPKCS7.setExternalDigest(extSignature, (byte[])null, "RSA");
                byte[] encodedSig = this.pdfPKCS7.getEncodedPKCS7(this.hash, this.tsaClient, this.ocsp, this.crlBytes, this.sigtype);
                if (this.estimatedSize + 2 < encodedSig.length) {
                    LOGGER.error("appendSignature: Not enough space");
                } else {
                    byte[] paddedSig = new byte[this.estimatedSize];
                    System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
                    PdfDictionary fdfDictionary = new PdfDictionary();
                    fdfDictionary.put(PdfName.CONTENTS, (new PdfString(paddedSig)).setHexWriting(true));
                    this.appearance.close(fdfDictionary);
                    result = true;
                }
            } else {
                LOGGER.error("appendSignature: parameters fail");
            }
        } catch (Exception ex) {
            LOGGER.error("appendSignature:" + ex.getMessage());
        }
        LOGGER.info("appendSignature: end------------------------------------");
        return result;
    }

    public Long getErrorCode() {
        return this.errorCode;
    }
}
