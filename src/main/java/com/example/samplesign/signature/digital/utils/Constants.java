package com.example.samplesign.signature.digital.utils;

public class Constants {
    public Constants() {
    }

    public interface DIGITAL_SIGN_RESULT_CODE {
        Long ERR_CODE_INIT_SUCSESS = 1L;
        String ERR_MESS_INIT_SUCSESS = "Khởi tạo thành công";
        Long ERR_CODE_HASH_SUCSESS = 1L;
        String ERR_MESS_HASH_SUCSESS = "Thực hiện hash thành công";
        Long ERR_CODE_SIGN_SUCSESS = 1L;
        String ERR_MESS_SIGN_SUCSESS = "Thực hiện ký thành công";
        Long ERR_CODE_SEVITICATE_NOTFOUND = 1581111L;
        String ERR_MESS_SEVITICATE_NOTFOUND = "Lỗi! chưa cấp chứng thư";
        Long ERR_CODE_SERIAL_NOTFOUND = 1581112L;
        String ERR_MESS_SERIAL_NOTFOUND = "Lỗi! không có thông tin serial chứng thư";
        Long ERR_CODE_DOC_PROCESSED = 31L;
        String ERR_MESS_DOC_PROCESSED = "Lỗi! văn bản đã được xử lý từ trước";
        Long ERR_CODE_FILE_NOT_EXIST = 1581114L;
        String ERR_MESS_FILE_NOT_EXIST = "Lỗi! file kí chính không tồn tại";
        Long ERR_CODE_FILE_SERVITICATE_NOT_EXIST = 1581115L;
        String ERR_MESS_FILE_SERVITICATE_NOT_EXIST = "Lỗi! file chứng thư không tồn tại";
        Long ERR_CODE_HASH_NOTSUCSESS = 1581117L;
        String ERR_MESS_HASH_NOTSUCSESS = "Lỗi! Thực hiện hash file";
        Long ERR_CODE_SIGNHASH_NOTSUCSESS = 1581118L;
        String ERR_MESS_SIGNHASH_NOTSUCSESS = "Lỗi! Thực hiện ký file từ client gửi lên";
        Long ERR_CODE_SIGNHASH_SEVITICATEFAILPASS = 1581119L;
        String ERR_MESS_SIGNHASH_SEVITICATEFAILPASS = "Lỗi! Chứng thư lưu trữ trên server bị lỗi";
        Long ERR_CODE_SIGNHASH_LENGFAIL = 15811110L;
        String ERR_MESS_SIGNHASH_LENGFAIL = "Lỗi! Độ dài chữ ký không đúng";
        Long ERR_CODE_CERTIFICATENOTFOUND = 15811111L;
        String ERR_MESS_SEVITICATENOTFOUND = "Lỗi! Không lấy được thông tin chứng thư trước khi ký";
        Long ERR_CODE_UPDATEFILESIGNURL_ERRDATA = 15811112L;
        String ERR_MESS_UPDATEFILESIGNURL_ERRDATA = "Lỗi! Lưu trữ file chữ ký không thành công";
        Long ERR_CODE_SIGNHASHATTACHFILEERR = 15811113L;
        String ERR_MESS_SIGNHASHATTACHFILEERR = "Lỗi! Trong quá trình thực hiện đính file chữ ký";
        Long ERR_CODE_FILECERTNOTACTIVE = 15811114L;
        String ERR_MESS_FILECERTNOTACTIVE = "Lỗi! Chưa kích hoạt chứng thư";
        int ERR_CODE_NEWSCREATEFILECER = 15811115;
        String ERR_MESS_NEWSCREATEFILECER = "Lỗi! Đang tồn tại một yêu cầu đang chờ cấp cer hoặc chưa nhập mã otp";
        Long ERR_CODE_CERPENDING = 15811116L;
        String ERR_MESS_CERPENDING = "Lỗi! Chứng thư đang bị tạm ngưng sử dụng";
        Long ERR_CODE_CERREMOVE = 15811117L;
        String ERR_MESS_CERREMOVE = "Lỗi! Chứng thư bị thu hồi";
    }

    public interface TEXT_PROCESS_DEFINE {
        int TEXT_ACTION_STATE_LD_SIGNER = 4;
        long SIGN_FROM_SIM = 2L;
        int TEXT_STATE_NEW_CREATE = 0;
        int TEXT_STATE_PROCESSING = 1;
        int TEXT_STATE_REJECTED = 2;
        int TEXT_STATE_APPROVED = 3;
        int TEXT_STATE_PUBLISHED = 4;
        int TEXT_STATE_SIGNDRAFF = 5;
        int TEXT_STATE_IS_CANCEL = 6;
        int TEXT_STATE_SIGNDRAFF_COMPLETE = 7;
        int TEXT_ACTION_STATE_NOT_RESPOND = 0;
        int TEXT_ACTION_STATE_VT_REJECTED = 1;
        int TEXT_ACTION_STATE_LD_REJECTED = 2;
        int TEXT_ACTION_STATE_VT_SIGNER = 3;
        int TEXT_STATE_SEND_TO_DEPARTMENT = 1;
        int TEXT_STATE_SEND_DEPARTMENT_SIGNED = 2;
        int TEXT_STATE_SEND_DEPARTMENT_REJECTED = 3;
        int TEXT_STATE_SEND_TO_CENTER = 8;
        int TEXT_STATE_SEND_TO_CENTER_SIGNED = 9;
        int TEXT_STATE_SEND_TO_CENTER_REJECTED = 10;
        int TEXT_STATE_SEND_TO_COMPANY = 16;
        int TEXT_STATE_SEND_TO_COMPANY_SIGNED = 17;
        int TEXT_STATE_SEND_TO_COMPANY_REJECTED = 18;
        int TEXT_STATE_SEND_TO_CORPORATION = 24;
        int TEXT_STATE_SEND_TO_CORPORATION_SIGNED = 25;
        int TEXT_STATE_SEND_TO_CORPORATION_REJECTED = 26;
        int TEXT_PROCESS_STATE_NEW = 0;
        int TEXT_PROCESS_STATE_REJECT = 2;
        int TEXT_PROCESS_STATE_APPROVE = 4;
        int TEXT_STATE_APPROVED_CANCEL = 27;
        long SIGN_FROM_APPROVED_DOC = 0L;
        long SIGN_FROM_SIM_PKCS2 = 4L;
    }

    public interface REJECT_SIGN_RESULT_CODE {
        String REJECT_SUCCESS = "1";
        String NO_REJECT_SUCCESS = "-1";
    }

    public interface SIGN_RESULT_CODE {
        Long SUCCESS = 1L;
        Long FILE_NOT_EXIST = 2L;
        String FILE_NOT_EXIST_TEXT = "Lỗi! File không tồn tại";
        Long SIGN_MSSP_ERROR = 3L;
        Long CA_VERIFY_NOT_CORRECT = 4L;
        Long UPDATE_DB_ERROR = 5L;
        Long NO_SIM_CA = 6L;
        String NO_SIM_CA_TEXT = "Lỗi! Người dùng không có CA SIM";
        Long NO_CMND_USER = 7L;
        Long NO_SIM_SERIAL = 8L;
        String NO_SIM_SERIAL_TEXT = "Lỗi! Không có serial Sim";
        Long FILE_SIGNED = 9L;
        Long CERTEIFICATE_CREATE = 10L;
        String CERTEIFICATE_CREATE_TEXT = "Lỗi! không lấy được chứng thư";
        Long NO_SUCCESS = 11L;
        Long STATUS_DELAY_SIGN = 417L;
        Long ERROR_EXPIRE_CER = 12L;
        Long ERROR_CER_BITHUHOI = 13L;
        Long MSSP_NO_CONNECTION = 14L;
        Long MSSP_TIMEOUT = 15L;
        Long NO_PERMISS_SIGN_MULTI = 16L;
        Long NO_SUCCESS_FULL = 17L;
        Long UNKNOWN_CLIENT = 18L;
        Long NOT_ENOUGH_SPACE = 19L;
        Long USER_NOT_SIGN = 20L;
        Long FILE_FORMAT_ERROR = 21L;
        Long SIGNATURE_FORMAT_ERROR = 22L;
        Long FILE_IS_NOT_SIGNED = 23L;
        Long EXPIRE_CERTIFICATE = 24L;
        Long REVOKED_CERTIFICATE = 25L;
        Long CERTIFICATE_IS_NOT_SUPPORT_CHECK_ONLINE = 26L;
        Long UNKNOWN_ONLINE_STATUS = 27L;
        Long CANNOT_CONNECT_VIETTELCA = 28L;
        Long WRONG_SIGNATURE_LENGTH = 29L;
        Long WRONG_CER_FORMAT = 30L;
        Long DOC_IS_PROCESS = 31L;
        Long WRONG_CRT_PASSWORD = 57005L;
        Long EXPIRE_CERTIFICATE_IN1MONTH = 32L;
        Long MSSPV20_COMPLETE = SUCCESS;
        Long MSSPV20_REQUESTOK = 2001L;
        Long MSSPV20_INPUT_INVALID = SIGN_MSSP_ERROR;
        Long MSSPV20_MSISDN_INVALID = NO_SIM_CA;
        Long MSSPV20_MSISDN_WRONG = NO_SIM_CA;
        Long MSSPV20_PROCESSCODE_INVALID = NO_SUCCESS;
        Long MSSPV20_MSG_DUPLICATED = NO_SUCCESS;
        Long MSSPV20_MSSFORMAT_NOTSUPPORTED = NO_SUCCESS;
        Long MSSPV20_MSGMODE_NOTSUPPORTED = NO_SUCCESS;
        Long MSSPV20_CERT_INVALID = UNKNOWN_CLIENT;
        Long MSSPV20_CERT_SERIAL_INVALID = ERROR_EXPIRE_CER;
        Long MSSPV20_HASH_INVALID = SIGNATURE_FORMAT_ERROR;
        Long MSSPV20_CLIENT_FORBIDDEN = UNKNOWN_CLIENT;
        Long MSSPV20_HINHTHUCKY_INVALID = NO_SUCCESS;
        Long MSSPV20_CLIENT_UNAUTHORIZED = UNKNOWN_CLIENT;
        Long MSSPV20_USER_CANCELED = USER_NOT_SIGN;
        Long MSSPV20_TRANS_CANCELED = USER_NOT_SIGN;
        Long MSSPV20_TRANS_ALREADY_CANCELED = USER_NOT_SIGN;
        Long MSSPV20_TRANS_NOTFOUND = NO_SUCCESS;
        Long MSSPV20_SENTTOSIM_FAILED = SIGN_MSSP_ERROR;
        Long MSSPV20_SENTTOSIM_FAILED_FULLMEM = SIGN_MSSP_ERROR;
        Long MSSPV20_MSISDN_UNREGISTERED = NO_SIM_CA;
        Long MSSPV20_CANNOT_SENTRESULT_TO_CLIENT = NO_SUCCESS_FULL;
        Long MSSPV20_SERVICE_NOTAVAIABLE = MSSP_NO_CONNECTION;
        Long MSSPV20_SYSTEM_UPGRADING = MSSP_NO_CONNECTION;
        Long MSSPV20_OTHER_UNKNOWN = SIGN_MSSP_ERROR;
        Long MSSPV20_OTHER_TIMEOUT = MSSP_TIMEOUT;
        Long MSSPV20_OTHER_NORESP = SIGN_MSSP_ERROR;
        Long MSSPV20_OTHER_NOSIGNATURE = SIGNATURE_FORMAT_ERROR;
        Long MSSPV20_OTHER_INVALIDSIGNATURE = SIGNATURE_FORMAT_ERROR;
        Long VERIFY_SIGNATURE_FALSE = 99L;
    }
}
