package com.example.samplesign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSignApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleSignApplication.class, args);
    }

}
