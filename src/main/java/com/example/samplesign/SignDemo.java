package com.example.samplesign;


import com.example.samplesign.signature.digital.DigitalSign;
import com.example.samplesign.signature.digital.utils.DigitalSignUtils;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.X509Certificate;

public class SignDemo {
    private static boolean useTSA =  true;
    private static boolean useLTV = true;
    private static String fullFileName = "data/sample.pdf";
    private static String outputFileName = "results/sample_signed.pdf";
    private static String outputFileNameLTV = "results/sample_signed_ltv.pdf";
    public static void main(String[] args) throws IOException, GeneralSecurityException, OperatorCreationException, PKCSException, DocumentException {
        DigitalSign digitalSign = new DigitalSign(null,fullFileName,outputFileName,false);
        X509Certificate x509Cert = DigitalSignUtils.getCertificateFromCerFile("cert/test.crt");
        X509Certificate[] chain = new X509Certificate[]{x509Cert};

        //embed timestamp
        if (useTSA) {
            TSAClient tsaClient = new TSAClientBouncyCastle("http://ts.ssl.com",null, null);
            digitalSign.setTsaClient(tsaClient);
        }

        // lay hash
        byte[] hash = digitalSign.getDigest(chain,"Test","Ha Noi", MakeSignature.CryptoStandard.CMS);

        // ky hash
        PrivateKey privateKey = DigitalSignUtils.getPrivateKey("cert/test.key","123456");
        byte[] signature = getSignature(hash,privateKey);
//        byte[] signature = signHash(hash,privateKey);
        // append chu ky
        boolean appendSig = digitalSign.appendSignature(signature);

        // add LTV
        if (appendSig && useLTV) {
            TSAClient tsa = new TSAClientBouncyCastle("http://ts.ssl.com",null, null, 6500, "SHA512");
            OcspClient ocsp = new OcspClientBouncyCastle(null);
            DigitalSignUtils.addLtv(outputFileName,outputFileNameLTV,ocsp, new CrlClientOnline(), tsa);
        }

    }

    // thuc hien ky, trong thuc te se duoc ky bang device
    private static byte[] getSignature(byte[] hash,PrivateKey pk) throws GeneralSecurityException {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        ExternalSignature externalSignature = new PrivateKeySignature(pk, DigestAlgorithms.SHA256, provider.getName());
        return externalSignature.sign(hash);
    }

    // ky hash, tuong duong voi getSignature
    private static byte[] signHash(byte[] hash,PrivateKey pk) throws GeneralSecurityException {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        String signMode = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigests(DigestAlgorithms.SHA256)) + "with" + pk.getAlgorithm();
        Signature sig;
        sig = Signature.getInstance(signMode, provider);
        sig.initSign(pk);
        sig.update(hash);
        return sig.sign();
    }
}
